import argparse
import collections


def getArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-port", help="Please enter a port to connect with server.", required=True)
    parser.add_argument("-host", help="Please enter a host to connect with server.", required=False,
                        default="127.0.0.1")
    arguments = parser.parse_args()
    return arguments


def parseDict(rawDict):
    dictKeys = rawDict.keys()
    if len(dictKeys) != 1:
        return False
    if 'Message' not in dictKeys:
        return False

    message = rawDict['Message']
    if len(message.keys()) != 3:
        return False
    if 'Body' in message.keys() and 'Header' in message.keys() and 'Title' in message.keys():
        if type(message.get('Body')) is not str and type(message.get('Body')) is not unicode:
            return False
        if type(message.get('Title')) is not str and type(message.get('Title')) is not unicode:
            return False
        if type(message.get('Header')) is dict or type(message.get('Header')) is collections.OrderedDict:
            header = message.get('Header')
            if len(header.keys()) != 3:
                return False
            if 'From' in header.keys() and 'Timestamp' in header.keys() and 'To' in header.keys():
                if type(header.get('From')) is not str and type(header.get('From')) is not unicode:
                    return False
                if type(header.get('To')) is not str and type(header.get('To')) is not unicode:
                    return False
                if type(header.get('Timestamp')) is not str and type(header.get('Timestamp')) is not unicode:
                    return False
            else:
                return False
        else:
            return False
    else:
        return False

    return True


def getDate(rawDate):
    date = rawDate.split('.')
    date.reverse()
    date = '-'.join(date)
    return date


def sortByDate(buffer, tempDate, container):
    if not buffer:
        for element in list(container.queue):
            message = element['Message']
            header = message.get('Header')
            timestamp = header.get('Timestamp')
            timestamp = timestamp.split('T')
            if tempDate == timestamp[0]:
                buffer.append(message)
        return buffer
    else:
        newBuffer = []
        for element in buffer:
            header = element['Header']
            timestamp = header.get('Timestamp')
            timestamp = timestamp.split('T')
            if tempDate == timestamp[0]:
                newBuffer.append(element)
        return newBuffer


def sortByFrom(buffer, tempFrom, container):
    if not buffer:
        for element in list(container.queue):
            message = element['Message']
            header = message.get('Header')
            varFrom = header.get('From')
            if varFrom == tempFrom:
                buffer.append(message)
        return buffer
    else:
        newBuffer = []
        for element in buffer:
            header = element['Header']
            varFrom = header.get('From')
            if varFrom == tempFrom:
                newBuffer.append(element)
        return newBuffer


def sortByTo(buffer, tempTo, container):
    if not buffer:
        for element in list(container.queue):
            message = element['Message']
            header = message.get('Header')
            varTo = header.get('To')
            if varTo == tempTo:
                buffer.append(message)
        return buffer
    else:
        newBuffer = []
        for element in buffer:
            header = element['Header']
            varTo = header.get('To')
            if varTo == tempTo:
                newBuffer.append(element)
        return newBuffer


def sortByTitle(buffer, tempTitle, container):
    if not buffer:
        for element in list(container.queue):
            message = element['Message']
            varTitle = message.get('Title')
            if varTitle == tempTitle:
                buffer.append(message)
        return buffer
    else:
        newBuffer = []
        for element in buffer:
            varTitle = element['Title']
            if varTitle == tempTitle:
                newBuffer.append(element)
        return newBuffer


def parseFilter(buffer, contentDict, container):
    jsonKeys = contentDict.keys()
    if len(jsonKeys) != 1:
        return False
    if 'filter' not in jsonKeys:
        return False

    filtersFields = ['to', 'from', 'date', 'title']
    varFilter = contentDict['filter']
    for field in varFilter.keys():
        if field not in filtersFields:
            return False
    if len(varFilter.keys()) == 0:
        return False

    if 'date' in varFilter.keys():
        date = varFilter.get('date')
        newDate = getDate(date)
        buffer = sortByDate(buffer, newDate, container)
        if not buffer:
            return buffer

    if 'from' in varFilter.keys():
        tempFrom = varFilter.get('from')
        buffer = sortByFrom(buffer, tempFrom, container)
        if not buffer:
            return buffer

    if 'to' in varFilter.keys():
        tempTo = varFilter.get('to')
        buffer = sortByTo(buffer, tempTo, container)
        if not buffer:
            return buffer

    if 'title' in varFilter.keys():
        tempTitle = varFilter.get('title')
        buffer = sortByTitle(buffer, tempTitle, container)
        if not buffer:
            return buffer

    return buffer
