import requests
import xmltodict
import pytest


@pytest.mark.parametrize("port", [(1234)])
def test_send_invalid_format(port):
    url = 'http://127.0.0.1:{0}/sendMessage'.format(port)
    dataToSend = """<?xml version="1.0" encoding="UTF-8"?>
    <Message>
        <Header>
            <To>Ivan</To>
            <From>Petr</From>
            <Timestamp>2019-08-03T17:21:24</Timestamp>
        </Header>
        <ad>HI</ad>
        <Title>Title</Title>
        <Body>Long long long text..
        </Body>
    </Message>
    """
    headers = {"Content-Type": "application/xml"}
    response = requests.post(url, data=dataToSend, headers=headers)
    assert response.text == "415 UNSUPPORTED MEDIA TYPE"


@pytest.mark.parametrize("port", [(1234)])
def test_send_valid_format(port):
    url = 'http://127.0.0.1:{0}/sendMessage'.format(port)
    dataToSend = """<?xml version="1.0" encoding="UTF-8"?>
    <Message>
        <Header>
            <To>Ivan</To>
            <From>Petr</From>
            <Timestamp>2019-08-03T17:21:24</Timestamp>
        </Header>
        <Title>Title</Title>
        <Body>Long long long text..
        </Body>
    </Message>
    """
    headers = {"Content-Type": "application/xml"}
    response = requests.post(url, data=dataToSend, headers=headers)
    assert response.text == ""


@pytest.mark.parametrize("port", [(1234)])
def test_get_message(port):
    url = 'http://127.0.0.1:{0}/getMessage'.format(port)
    dataToGet = """<?xml version="1.0" encoding="UTF-8"?>
    <Message>
        <Header>
            <To>Ivan</To>
            <From>Petr</From>
            <Timestamp>2019-08-03T17:21:24</Timestamp>
        </Header>
        <Title>Title</Title>
        <Body>Long long long text..
        </Body>
    </Message>
    """
    dataToGet = xmltodict.parse(dataToGet, encoding="UTF-8")
    response = requests.get(url)
    response = xmltodict.parse(response.text, encoding="UTF-8")
    assert response == dataToGet


@pytest.mark.parametrize("port", [(1234)])
def test_get_message_from_empty_queue(port):
    url = 'http://127.0.0.1:{0}/getMessage'.format(port)
    response = requests.get(url)
    assert response.text == "404 NOT FOUND"


@pytest.mark.parametrize("port", [(1234)])
def test_send_invalid_filter_format(port):
    url = 'http://127.0.0.1:{0}/findMessage'.format(port)
    filterToSend = """{"filter": {"body": "Title", "from": "Petr"}}"""
    headers = {"Content-Type": "application/json"}
    response = requests.post(url, data=filterToSend, headers=headers)
    assert response.text == "415 UNSUPPORTED MEDIA TYPE"


@pytest.mark.parametrize("port", [(1234)])
def test_not_found_messages(port):
    url = 'http://127.0.0.1:{0}/findMessage'.format(port)
    filterToFind = """{"filter": {"title": "Not found messages", "from": "Petr"}}"""
    headers = {"Content-Type": "application/json"}
    response = requests.post(url, data=filterToFind, headers=headers)
    assert response.text == "404 NOT FOUND"


@pytest.mark.parametrize("port", [(1234)])
def test_find_messages(port):
    urlSend = 'http://127.0.0.1:{0}/sendMessage'.format(port)
    urlFind = 'http://127.0.0.1:{0}/findMessage'.format(port)
    dataToSend = """<?xml version="1.0" encoding="UTF-8"?>
     <Message>
        <Header>
            <To>Ivan</To>
            <From>Petr</From>
            <Timestamp>2019-08-03T17:21:24</Timestamp>
        </Header>
        <Title>Title</Title>
        <Body>Long long long text..
        </Body>
    </Message>
    """
    dataForFind = """<?xml version="1.0" encoding="UTF-8"?>
    <Messages>
        <Message>
            <Header>
                <To>Ivan</To>
                <From>Petr</From>
                <Timestamp>2019-08-03T17:21:24</Timestamp>
            </Header>
            <Title>Title</Title>
            <Body>Long long long text..
            </Body>
        </Message>
    </Messages>
    """
    headersForSend = {"Content-Type": "application/xml"}
    requests.post(urlSend, data=dataToSend, headers=headersForSend)

    dataForFind = xmltodict.parse(dataForFind, encoding="UTF-8")
    filterForFind = """{"filter": {"title": "Title", "from": "Petr"}}"""
    headersForFind = {"Content-Type": "application/json"}
    response = requests.post(urlFind, data=filterForFind, headers=headersForFind)
    response = xmltodict.parse(response.text, encoding="UTF-8")
    assert response == dataForFind
