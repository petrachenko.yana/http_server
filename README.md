Задание:
В качестве задания нужно было реализовать HTTP-сервер для хранения очереди запросов (FIFO) со следующим API запросов:
1.	sendMessage – принимает в качестве тела сообщения XML следующего формата
```
<?xml version="1.0" encoding="UTF-8"?>
<Message>
	<Header>
		<To>Ivan</To>
		<From>Petr</From>
		<Timestamp>2019-08-03T17:21:24</Timestamp>
	</Header>
	<Title>Title</Title>
	<Body>Long
	long
	long
	text..
	</Body>
</Message>
```
и возвращает пустой ответ в случае успешного помещения сообщения в очередь.
2.	getMessage – принимает пустой запрос и возвращает XML со следующим сообщением в очереди. Сообщение из очереди удаляется и в последствии не будет найдено методом findMessages
3.	findMessages – принимает в качестве тела сообщения JSON следующего формата
```
{
  "filter": {
	"from":"Petr",
	"date":"31.12.2018"
  }
}
```
и возвращает результаты выбора сообщений, находящихся в очереди, по фильтру (отправитель, получатель, дата, заголовок) в виде
```
<?xml version="1.0" encoding="UTF-8"?>
    <Messages>
        <Message>
            <Header>
                <To>Ivan</To>
                <From>Petr</From>
                <Timestamp>2019-08-03T17:21:24</Timestamp>
            </Header>
            <Title>Title</Title>
            <Body>Long long long text..
            </Body>
        </Message>
		<Message>
            <Header>
                <To>Yana</To>
                <From>Petya</From>
                <Timestamp>2019-08-03T17:21:24</Timestamp>
            </Header>
            <Title>Title1</Title>
            <Body>Long long long text..
            </Body>
        </Message>
    </Messages>
```
В качестве дополнительных утилит для запроса sendMessage был написан парсер XML сообщения, который проверяет соответсвие сообщения заданному формату.
Для запроса findMessages написана выборка сообщений по четырем тэгам: date, to, from, title. Можно отсортировать сообщения, используя как все четыре поля, так и какое-то одно из них.

Запуск:
При запуске скрипта аргумент `-port` является обязательным, а аргумент `-host` - нет, по умолчанию он стоит "127.0.0.1".  
Запустить скрипт можно следующей командой - `python server.py -port 1234` или `python server.py -port 12 -host "127.0.1.12"`  
Примеры взаимодействия с функциями с помощью curl:
```
curl -X POST http://127.0.0.1:1234/sendMessage -H "Content-Type: text/xml" -d @data.xml
curl -X GET http://127.0.0.1:1234/getMessage
curl -X POST http://127.0.0.1:1234/findMessage -H "Content-Type: text/json" -d @data.json
```

Запуск тестов, написанных с помощью `pytest`:  
`- pytest test_server.py`