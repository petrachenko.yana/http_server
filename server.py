import utils
from flask import Flask, request, Response
from Queue import Queue
from collections import OrderedDict
import xmltodict
from dicttoxml import dicttoxml
from xml.dom.minidom import parseString


app = Flask(__name__)
container = Queue(maxsize=0)


@app.route("/")
def greeting():
    return "Choose /sendMessage, /getMessage or /findMessage to interact with functions."


@app.route("/sendMessage", methods=["POST"])
def sendMessage():
    contentDict = xmltodict.parse(request.data)
    if utils.parseDict(contentDict) is True:
        container.put(contentDict)
        response = Response(status=200)
        return response
    else:
        response = Response(status=415)
        return response.status


@app.route("/getMessage", methods=["GET"])
def getMessage():
    if container.empty():
        response = Response(status=404)
        return response.status
    else:
        xml = dicttoxml(container.get(), attr_type=False, root=False)
        dom = parseString(xml)
        return dom.toprettyxml(encoding="UTF-8")


@app.route("/findMessage", methods=["GET", "POST"])
def findMessage():
    contentDict = eval(request.data)
    tempBuffer = []
    result = utils.parseFilter(tempBuffer, contentDict, container)
    if result is False:
        response = Response(status=415)
        return response.status

    if not result:
        response = Response(status=404)
        return response.status
    else:
        itemName = lambda x: "Message"
        template = OrderedDict({"Messages": [item for item in result]})
        xml = dicttoxml(template, item_func=itemName, attr_type=False, root=False)
        dom = parseString(xml)
        return dom.toprettyxml(encoding="UTF-8")


if __name__ == '__main__':
    arguments = utils.getArguments()
    port = int(arguments.port)
    host = arguments.host
    app.run(host=host, port=port)
